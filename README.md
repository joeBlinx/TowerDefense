
# Projet TowerDefense ZZ1 

Debut du projet : 03/02/2016 <br/>
Fin de projet : 31/05/2016 

## Membres : 

<ul>
<li>	Perquy Leonor </li>
<li>	Mottet Victor </li>
<li>	Lethuillier Alexis </li>
<li>	Jozereau Ludovic </li>
<li>	Lemoing Matthieu (la secrétaire) </li>
<li>	Aigle Stiven </li>
</ul>
## Techno : C++/OpenGL3.3 <br/>

## Bibliotheque:
<ul>
	<li>	SDL2 </li>
	<li>	SDL2-image </li>
	<li>	glm </li>
	<li>	boost </li>
</ul>