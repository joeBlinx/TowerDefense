//
// Created by stiven on 21/04/16.
//

#ifndef TOWERDEFENSE_GAME_HPP
#define TOWERDEFENSE_GAME_HPP

#include <SDL2/SDL_video.h>

bool play(int width, int height, SDL_Window *window, bool *bigRun);

#endif //TOWERDEFENSE_GAME_HPP
