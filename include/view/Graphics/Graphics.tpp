template <typename T>
void Graphics::displayRange(T *tower, int spell , float range, float center, int onPath ){

    if(!spell){
        range = tower->getRange();
    }
    vao.bind();
    Range.pictureSize = glm::vec2(1);// assigne un unif de la form vec2
    Range.onPath = onPath;
    // definition de la position et de la taille du quad support
    transformStruct.tran = tower->getPosition();
    transformStruct.scale = glm::vec2(range*2, range*2);
    Range.spell = spell;
    //glUniformMatrix3fv(uniTranRange, 1, false, glm::value_ptr(transform(transformStruct, 0.5, 0.5)));    // coord a partir du coin HG
    if(center < 0.5f){
        center = (range - 25.0f)/(range*2);
    }
    Range.tran = libgl::transform(transformStruct,center,center);

    // affichage du quad support : precision dans le fragment shader
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //0 : premier vertex, 4 : quad = 4 vertices
}

template <typename T>
void Graphics::displayLife(T &enemy, glm::vec2 scale ) {
    if (enemy.getHp() != enemy.getHpMax()) {
        transformStruct.tran = enemy.getPosition();
        transformStruct.tran.y -= enemy.getSize() / 2;
        transformStruct.scale = scale;
        Barre.tran = libgl::transform(transformStruct, 0.5, 0.5);
        Barre.hp = enemy.getHp();
        Barre.hpMax = enemy.getHpMax();
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        libgl::getError();
    }

}
