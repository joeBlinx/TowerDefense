//
// Created by lysilia on 04/04/16.
//

#ifndef TOWERDEFENSE_GRAPHICSMENUTOWER_H
#define TOWERDEFENSE_GRAPHICSMENUTOWER_H

#include <vector>
#include <string>
#include <GLifier/macro.hpp>
#include <GLifier/Program.hpp>
#include <GLifier/Vao.hpp>
#include <GLifier/Vbo.hpp>
#include <GLifier/quad.hpp>
#include <GLifier/Texture.hpp>
#include <GLifier/displayText.hpp>
#include <view/userInterface/Button.hpp>



//constexpr float decaleUv = 0.25f;

extern const float nbPictureLine;
extern const float nbPictureColumn;

extern const float widthPictureMenuTower;
extern const float heightPictureMenuTower;

class GraphicsMenuTower {
private:
    libgl::Vao vao;
    libgl::Vbo vboRect;
    libgl::Vbo vboUv;

    LIBGL_CREATE_PROGRAM(program, "../sources/view/shader/VertexMenuTower.glsl",
                         "../sources/view/shader/FragmentMenuTower.glsl",
                         (scale)(transformation)(texture2D)(surbrillance)(alpha)(hg)(bd))

    libgl::Texture texture_menuTower;
    libgl::transformStorage transformStruct;

    int width;
    int height ;

public:
    GraphicsMenuTower(std::string pathTexture, int widthWindow, int heightWindow);
    void displayButtons(Button &button, int idBtn);
    void changeAlpha(float alpha);
    void changeColor (glm::vec3 color);
    void displayRect();



};


#endif //TOWERDEFENSE_GRAPHICSMENUTOWER_H
