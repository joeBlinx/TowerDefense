//
// Created by hole on 10/04/16.
//


#ifndef TOWERDEFENSE_ONECLICPROJO_H
#define TOWERDEFENSE_ONECLICPROJO_H

#include "Spell.h"

class Meteor : public Spell{
private:
    int nbProjo;
    Path &path;
public:
    bool apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
               const std::vector<std::unique_ptr<Enemy>> &enemies,
               std::vector<std::unique_ptr<Projectile>> &projectiles) override;

    Meteor(int price,
                    int coolDown,
                    utils::element element,
                    float damage,
                    std::vector<std::shared_ptr<Buff>> buffList,int nbEnemy,Path &path);


};
#endif //TOWERDEFENSE_ONECLICPROJO_HPP
