//
// Created by hole on 29/03/16.
//

#ifndef TOWERDEFENSE_ONECLICMIROIR_H
#define TOWERDEFENSE_ONECLICMIROIR_H

#include <engine/path/Path.h>
#include "Spell.h"

class OneClicMiroir : public Spell{
private:

    Path &path;
public:
    bool apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
               const std::vector<std::unique_ptr<Enemy>> &enemies,
               std::vector<std::unique_ptr<Projectile>> &projectiles) override;



    OneClicMiroir(int price, int coolDown, utils::element element, float damage, Path &path);

};
#endif //TOWERDEFENSE_ONECLICPROJO_H
