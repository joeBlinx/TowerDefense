//
// Created by hole on 29/03/16.
//

#ifndef TOWERDEFENSE_ZONEEFFECT_H
#define TOWERDEFENSE_ZONEEFFECT_H

#include "Spell.h"



class ZoneEffect : public Spell{
private:
    float range;
    bool forTower = false;
//    bool needTarget=true;
    bool active = false;

public:
    bool apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
               const std::vector<std::unique_ptr<Enemy>> &enemies,
               std::vector<std::unique_ptr<Projectile>> &projectiles)  override;



    ZoneEffect(int price,
               int coolDown,
               utils::element element,
               float damage,
               std::vector<std::shared_ptr<Buff>> buffList,
               float range);

    ZoneEffect(int price,
               int coolDown,
               utils::element element,
               float damage,
               std::vector<std::shared_ptr<Buff>> buffList,
               float range, bool forTower);

    float getRange() override;
};

#endif //TOWERDEFENSE_ZONEEFFECT_H
