//
// Created by hole on 29/03/16.
//

#ifndef TOWERDEFENSE_ONECLICEFFECT_H
#define TOWERDEFENSE_ONECLICEFFECT_H

#include "Spell.h"

class OneClicEffect : public Spell{
private:
    bool forTower = false;
public:
    bool apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
               const std::vector<std::unique_ptr<Enemy>> &enemies,
               std::vector<std::unique_ptr<Projectile>> &projectiles)  override;

    OneClicEffect(int price, int coolDown, utils::element element, float damage,
                      std::vector<std::shared_ptr<Buff>> buffList, bool forTower = false);

};

#endif //TOWERDEFENSE_ONECLICEFFECT_H
