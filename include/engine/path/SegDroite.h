//
// Created by hole on 12/03/16.
//

#ifndef TOWERDEFENSE_SEGDROITE_H
#define TOWERDEFENSE_SEGDROITE_H
#include <glm/glm.hpp>
#include "SegmentMere.h"

class SegDroite : public SegmentMere {
    glm::vec2 Pos1;
    glm::vec2 Pos2;

public:
    float longueur();
    glm::vec2 calculPos(float t) override ;

    glm::vec2 calculPosReele(float t) override ;

    SegDroite();
    SegDroite(glm::vec2 Pos1, glm::vec2 Pos2);
};


#endif //TOWERDEFENSE_SEGDROITE_H
