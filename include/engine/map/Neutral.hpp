//
// Created by jozereau on 21/03/16.
//

#pragma once

#include <glm/vec2.hpp>
#include <utils.hpp>
#include "Object.hpp"

class Neutral {

protected:
    const glm::vec2 pos;
    const float modifier;
    const utils::element type;
    Object * object;

public:
    Neutral(glm::vec2 pos, float modifer, utils::element type, Object * object);

    const glm::vec2 getPosition();

    const float getModifier();

    const utils::element getType();

    Object * getObject();
    void destruct();
};
