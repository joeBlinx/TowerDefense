//
// Created by jozereau on 15/02/16.
//

#pragma once

#include "Tower.hpp"

class Water : public Tower {

public:
    Water();
    Water(glm::vec2 pos);
    std::unique_ptr <Projectile> shoot(Path &path, float gameSpeed) override ;
};