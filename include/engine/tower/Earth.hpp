//
// Created by jozereau on 15/02/16.
//

#pragma once

#include "Tower.hpp"

class Earth : public Tower {

public:
    Earth();
    Earth(glm::vec2 pos);
    std::unique_ptr <Projectile> shoot(Path &path, float gameSpeed) override ;
};