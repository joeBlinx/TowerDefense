//
// Created by stiven on 21/05/16.
//

#ifndef TOWERDEFENSE_METEOREPROJ_HPP
#define TOWERDEFENSE_METEOREPROJ_HPP

#include "engine/projectile/Projectile.h"
class MeteoreProj : public Projectile{
private:
    int change = 0;
    int timeToChange = 5;
    void changeRotate();

public :
    MeteoreProj(glm::vec2 pos, glm::vec2 vit, glm::vec2 size, float damage, std::vector<std::unique_ptr<Buff>> &buff,
                             utils::element element,int countdown);


    void move(float accel) override;


};

#endif //TOWERDEFENSE_METEOREPROJ_HPP
