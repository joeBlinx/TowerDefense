//
// Created by stiven on 16/02/16.
//

#ifndef TOWERDEFENSE_CONTROLLERIU_HPP
#define TOWERDEFENSE_CONTROLLERIU_HPP


#include "view/userInterface/Button.hpp"
#include "view/userInterface/IUGraphic.hpp"
#include <vector>
#include <SDL2/SDL.h>
#include <view/Graphics/Graphics.hpp>
#include <view/userInterface/TowerButton.h>
#include "view/userInterface/SpellButton.hpp"

class Regis;
class ControllerIU {

protected:

    IUGraphic iuGraphic;
    std::vector<std::unique_ptr<Button>> button;
    std::vector <Button> buttonMenu;
    std::vector<SpellButton> spellButton;
    std::unique_ptr<SpellButton> spell = nullptr;


    Graphics *graph = nullptr;
    bool select = false;
    Regis *regis = nullptr;
    glm::vec3 colorAccel{0, 0, 0};
    int widthWindow;
    int heightWindow;
    int widthButton;
    Button end;


public:
    ControllerIU(int widthWindow, int heightWindow, int numberButton, Regis *regis, Graphics *graphics);

    ControllerIU(int width, int height);

    void display(const StatPlayer &stat, float cooldown, float cooldownMax, bool onPath);

    void checkButton(float x, float y);

    void deleteGhost();

    template <typename T>
    void addGhost(glm::vec2 pos, utils::element type);
    void callRegisCreateTower(utils::element type);
    void click();

    void setColorAccel();

    void decreaseCooldown();

    void createSpell(std::string key, int cooldown, float range);

    bool isSomethingSelected(int x, int y);

    bool isThereAGhost(){return select;}

    void displayMenu();
    void checkButtonMenu(float x, float y);
    void clickMenu();
    void clickEnd();
    void checkEnd(float x , float y);
    void findMiddle(Button &button);
    void animMenu();
    void resetPosMenu();
    void displayEnd(utils::state state);


};
#include "controller/ControllerIU.tpp"

#endif //TOWERDEFENSE_CONTROLLERIU_HPP
