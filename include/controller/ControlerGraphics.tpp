template<typename T>
void ControlerGraphics::display(const std::vector<std::unique_ptr<T> > &displayObject){
    for(auto &object : displayObject){
        glm::vec2 pos = (*object).getPosition();
        graphics.displayT(pos);
    }
}