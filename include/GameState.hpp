//
// Created by stiven on 08/03/16.
//

#ifndef TOWERDEFENSE_GAMESTATE_HPP
#define TOWERDEFENSE_GAMESTATE_HPP

#include <glm/glm.hpp>
#include <memory>
#include <vector>
#include <controller/ControllerSpell.h>
#include <fstream>
#include "engine/projectile/Projectile.h"
#include "engine/path/Path.h"
#include "game.hpp"


class Tower;
class Enemy;
constexpr int widthTower = 50 ;
constexpr int heightTower = 50;
struct StatPlayer{
    int hp = 10 ;
    int mana = 100;
    int gold = 200;
};

class GameState{
private:
    std::vector <std::unique_ptr <Tower> > towers;
    EnemyWave wave;
    std::vector<std::unique_ptr<Enemy>> enemies;
    std::vector <std::unique_ptr <Projectile> > projectile;
    std::vector <std::unique_ptr<Projectile>> aoe;
    int changeSprite ;
    bool acceleration = false;
    const int accelSprite = 5;
    const int valueAccel = 2;
    int accelGame= 1;
    Path &path;
    Path &airPath;
    int frameJeu = 0;
    int frameAccel = 0;
    std::vector <float> frameReel{0};// sert pour l'accéleration des projo, lorsqu'on en tire plus d'un par frame, du coup sert a rien lol
    StatPlayer statPlayer{};
    ControllerSpell controllSpell;
    bool launchWave = false;




public:

    GameState(Path &path, Path &airPath);
    int createTower(glm::vec2 carac, utils::element type);
    int play(utils::state &state);
    std::vector <std::unique_ptr <Tower> > & getTower();
    std::vector <std::unique_ptr <Enemy> > & getEnemy();
    std::vector <std::unique_ptr <Projectile> >& getProjectile();
    const StatPlayer & getStat() const ;
    const int getChangeSprite() const ;
    void changeAcceleration();
    void moveEnemies();
    int useSpell(std::string key, int x, int y);
    void moveProj();
    void addSpell(std::string key,std::unique_ptr<Spell> newSpell );
    void suppTower(Tower *tower);
    void cheat();
    void newWave(std::string nomFichier);
    void launch();
    vecptr<Projectile> &getAoe(){ return aoe;}
    void changeAoe();
    const int getAccelGame(){ return  accelGame;}
    const int getCooldownBeforeWave();
    const int getCooldownMax();
    bool testTowerOnTower(glm::vec2 carac);


};

#endif //TOWERDEFENSE_GAMESTATE_HPP
