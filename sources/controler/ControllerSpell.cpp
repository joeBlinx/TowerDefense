//
// Created by hole on 28/03/16.
//

#include <map>
#include <string>
#include "./controller/ControllerSpell.h"
#include <utility>

ControllerSpell::ControllerSpell(
                                 const std::vector<std::unique_ptr<Enemy>> &enemies,
                                 const std::vector<std::unique_ptr<Tower>> &towers,
                                  std::vector<std::unique_ptr<Projectile>> &projectiles)
                                            : enemies(enemies), towers(towers), projectiles(projectiles) {

}

bool ControllerSpell::applySpell(std::string key, float x, float y) {
    return spell[key]->apply(x, y,towers,enemies,projectiles);
}
void ControllerSpell::addSpell(std::string key, std::unique_ptr<Spell> newSpell){
    spell.insert(std::pair<std::string, std::unique_ptr<Spell>> (key, std::move(newSpell)));
}
int ControllerSpell::getMana(std::string key) {
   return spell[key]->getMana();
}