//
// Created by leo on 02/03/16.
//

#include "controller/ControlerGraphics.hpp"
#include "controller/Regis.hpp"
#include <iostream>

ControlerGraphics::ControlerGraphics(float widthWindow, float heightWindow, Path *path, Regis &regis) :
        graphics("../resources/images/sprites.png",
                 "../resources/images/proj.png", widthWindow, heightWindow,
                 "../resources/images/tour.png", path),
        effect(widthWindow, heightWindow),
        regis(regis) {
}


void ControlerGraphics::displayTower(const std::vector<std::unique_ptr<Tower> > &theTower) {
    for(auto &tower : theTower)
        graphics.displayTower(*tower);
}

void ControlerGraphics::displayRange(const std::vector<std::unique_ptr<Tower> > &theTower) {
    for(auto &tower : theTower)
        graphics.displayRange(tower.get());
}

void ControlerGraphics::displayOneRange(Tower *tower) {
    graphics.displayRange(tower);
}

void ControlerGraphics::displayEnemy(const std::vector<std::unique_ptr<Enemy> > &theEnemy) {

    for(auto &enemy : theEnemy) {
        graphics.displayEnemy(*enemy);

    }
}


void ControlerGraphics::displayProjectile(const std::vector<std::unique_ptr<Projectile> > &theProjectile, bool under) {
   
    for(auto &projectile : theProjectile) {
                if((projectile->getIsDetectedByEnemy() && under )|| !projectile->getIsDetectedByEnemy())
                    graphics.displayProjectile(*projectile, 0);
    }
}


void ControlerGraphics::spriteEnemy(const std::vector<std::unique_ptr <Enemy> > &theEnemy){
    for(auto &enemy : theEnemy) {
        graphics.spriteEnemy(enemy->getOrientation(), *enemy);

    }

}

void ControlerGraphics::displayPath() {
    graphics.displayPath();
}

void ControlerGraphics::displayBackground(int useTexture) {
    graphics.displayBackground(useTexture);
}


void ControlerGraphics::discretPath() {
    graphics.discretPath();
}


/**
 *      verifie si le curseur est sur une tour
 */
void ControlerGraphics::checkSelection(const std::vector<std::unique_ptr<Tower> > &theTower, float x , float y){

    for (auto &iter : theTower){
        iter.get()->setMouseSelection(iter.get()->isMouseSelection(x, y));


        // au survol de la souris
        /* if(iter.get()->getSelected()){
             displayOneRange(iter.get());
         }*/
    }

}

/**
 *      action du click gauche sur une tour
 *      une seule tour selectionne a la fois
 */
void ControlerGraphics::click(const std::vector<std::unique_ptr<Tower> > &theTower, int x, int y){



   for (auto &tower : theTower) {
       // si c'est la tour sur laquelle on a clique
       if (tower.get()->getMouseSelection()) {

           // si la tour est deja selectionne, on la deselectionne
           if(tower.get()->getSelected()){
               tower.get()->setSelected(!tower.get()->getSelected());
               // on supprime le menu
               regis.suppButtonsMenuTower();
           }

           // sinon, on change la tour selectionne
           else{
               if(selectedTower != nullptr){
                   selectedTower->setSelected(false);
                   // on supprime le menu
                   regis.suppButtonsMenuTower();
               }
               setSelectedTower(*(tower));
               selectedTower->setSelected(true);

                //creation des boutons
               regis.creatButtonsMenuTower(selectedTower->getPosition().x, selectedTower->getPosition().y);
           }

       }
   }
}





/**
 *      affichage de la range que si y'a eu un click
 */
void ControlerGraphics::displayRangeOnClick(const std::vector<std::unique_ptr<Tower> > &theTower){
    for(auto &tower : theTower) {
        if(tower.get()->getSelected()) {
            graphics.displayRange(tower.get());
        }
    }
}




/**
 * Vérifie s'il y a une tour sélectionnée
 */
bool ControlerGraphics::isATowerSelected(const std::vector<std::unique_ptr<Tower> > &theTower, int x, int y){
    bool resu = false;
    for(auto &iter : theTower){
        if(iter.get()->isMouseSelection(x, y))
            resu = true;
    }
    return resu;
}


void ControlerGraphics::setSelectedTower(Tower &tower){
    selectedTower = &tower;
}


Tower* ControlerGraphics::getSelectedTower(){
    return selectedTower;
}


void ControlerGraphics::changeScale(const float x, const float y) {
    graphics.changeScale(x, y);
    effect.changeScale(x, y);
}
void ControlerGraphics::displayFreeze(float timeExec) {
    effect.displayFreeze(timeExec);
}

void ControlerGraphics::razFreeze() {
    effect.razAlpha();
}

void ControlerGraphics::displayUpTower(const std::vector<std::unique_ptr<Tower>> &theTower) {
    static int use = 0;
    for(auto &tower : theTower) {
        if(tower->isUp()) {
            effect.displayUpTower(tower, use);
        }
    }
    use ++;
}


Graphics* ControlerGraphics::getGraphics(){
    return &graphics;
}

void ControlerGraphics::displayOndeDeChoc(vecptr<Projectile> &proj) {
    for (auto & projectile : proj){
        effect.displayOndeDeChoc(projectile);
    }

}

