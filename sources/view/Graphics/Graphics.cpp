//
// Created by leo on 22/02/16.
//


#include "view/Graphics/Graphics.hpp"
#include "engine/tower/Tower.hpp"
#include "utils.hpp"
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>
#include <GLifier/Uniform.hpp>


constexpr float numberWidthPicturesTour = 4.f;
constexpr float numberHeightPicturesTour = 2.f;

const float widthPictureTower =
        1.f / numberWidthPicturesTour;      // proportion largeur d'une tour par rapport a la largeur de l'image
const float heightPictureTower = 1.f / numberHeightPicturesTour;      // idem pour la hauteur


constexpr float numberWidthPicturesEnemy = 20.0f;     // nombre d'image par ligne
constexpr float numberHeightPicturesEnemy = 16.0f;    // nombre d'image par colonne

float widthPictureEnemy = 1.0f / numberWidthPicturesEnemy;
float heightPictureEnemy = 1.0f / numberHeightPicturesEnemy;


float widthPictureProjectile = 1/4.0f;
float heightPictureProjectile = 1.0f/2.0f;

Graphics::Graphics(std::string pathTextureEnemy, std::string pathTextureProjectile, int widthWindow,
                   int heightWindow, std::string pathTextureTower, Path *path) :
        textureTower(pathTextureTower), textureEnemy(pathTextureEnemy), textureProjectile(pathTextureProjectile),
        posHtextureEnemy(0), posVtextureEnemy(0), widthWindow(widthWindow), heightWindow(heightWindow), path(path) {
    vao.bind();
    // on init les vbo apres leur avoir binder le vao
    vboRect = libgl::Vbo(0, 3, libgl::mat::rect);  // y'a un carre deja cree :) 3D
    vboCoord = libgl::Vbo(1, 2, libgl::mat::uv);   // coordonne de chaque pixel ('2D')
    Range.scale = libgl::normalizedScreenSpace(widthWindow, heightWindow);
    // specification du program que l'on va utiliser
    // on place dans le shader via uniScale la mat 3x3 qui va normaliser l'espace ecran
    // (0,0) normalement au centre de l'ecran, on le place en haut a gauche
    display.scale = libgl::normalizedScreenSpace(widthWindow, heightWindow);
    display.change = 2.0f;
    // idem pour la range
    Barre.scale = libgl::normalizedScreenSpace(widthWindow, heightWindow);
    // uniUvHG est recalculer dans displayButtons  (template)
    vaoPath.bind();
    pathEnemy.scale = libgl::normalizedScreenSpace(widthWindow, heightWindow);
    vboRectPath = libgl::Vbo(1, 2, libgl::mat::uv);



}


Graphics::Graphics(int width, int height) : widthWindow(width), heightWindow(height) {
    vao.bind();
    // on init les vbo apres leur avoir binder le vao
    vboRect = libgl::Vbo(0, 3, libgl::mat::rect);  // y'a un carre deja cree :) 3D
    vboCoord = libgl::Vbo(1, 2, libgl::mat::uv);
    display.scale = libgl::normalizedScreenSpace(widthWindow, heightWindow);

}

Graphics::Graphics() : path() { }

void Graphics::discretPath() {
    for (int i = 0; i < path->getTotalLength(); i += 10) {
        pointPath.push_back(path->getPosition(i));
    }
    triangulize();

}





/**
 * Affichage d'une tour
 */
void Graphics::displayTower(Tower &tower) {
    // ce qu'il doit utiliser : on fait square par square
    vao.bind();
    textureTower.bindTexture();

    display.pictureSize = glm::vec2(widthPictureTower, heightPictureTower);// assigne un unif de la form vec2

    // on envoie la texture_menuTower a l'uniforme
    display.texture2D = 0;// 0 : num de texture_menuTower unit -> index pour le shader
    display.uvHG = utils::calculUv(tower);// fv : pointeur vers un tableau de float

    transformStruct.tran = tower.getPosition();
    transformStruct.scale = glm::vec2(50, 50);

    // coord a partir du coin HG
    display.tran = libgl::transform(transformStruct, 0.5, 0.5);
    // dessination du square
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //0 : premier vertex, 4 : quad = 4 vertices

}

/**
 * Affichage d'une range
 */
/*void Graphics::displayRange(Tower &tower){
    float range = tower.getRange();
    vao.bind();
    Range.pictureSize = glm::vec2(1);// assigne un unif de la form vec2

    // definition de la position et de la taille du quad support
    transformStruct.tran = tower.getPosition();
    transformStruct.scale = glm::vec2(range * 2, range * 2);

    //glUniformMatrix3fv(uniTranRange, 1, false, glm::value_ptr(transform(transformStruct, 0.5, 0.5)));    // coord a partir du coin HG
    Range.tran = libgl::transform(transformStruct, 0.5, 0.5);

    // affichage du quad support : precision dans le fragment shader
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //0 : premier vertex, 4 : quad = 4 vertices
}*/

/**
 * Affichage d'un projectile
 */
void Graphics::displayProjectile(Projectile &projectile, int useTexture) {
    // ce qu'il doit utiliser : on fait square par square
    vao.bind();
    textureProjectile.bindTexture();
    display.useTexture = useTexture;

    display.pictureSize = glm::vec2(widthPictureProjectile, heightPictureProjectile);// assigne un unif de la form vec2

    // on envoie la texture_menuTower a l'uniforme
    display.texture2D = 0;// 0 : num de texture_menuTower unit -> index pour le shader
    if(!useTexture) {
        display.uvHG = utils::calculUv(projectile);// fv : pointeur vers un tableau de float
    }else{
        display.uvHG = glm::vec2{0};
    }

    transformStruct.tran = projectile.getPosition();
    transformStruct.scale = glm::vec2(projectile.getSize());
    transformStruct.angle = projectile.getRotate();

    // coord a partir du coin HG
    display.tran = libgl::transform(transformStruct, 0.5, 0.5);
    // dessination du square
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //0 : premier vertex, 4 : quad = 4 vertices
    transformStruct.angle = 0.0f;

}


/**
 * Affichage d'un enemy
 */
void Graphics::displayEnemy(Enemy &enemy) {
    // ce qu'il doit utiliser : on fait square par square

    vao.bind();
    textureEnemy.bindTexture();


    display.pictureSize = glm::vec2(widthPictureEnemy, heightPictureEnemy);

    // on envoie la texture_menuTower a l'uniforme

    display.texture2D = 0;// 0 : num de texture_menuTower unit -> index pour le shader
    // vec(0,0) on place la lecture au coin en H a G

    /* A rajouter pour la version ok des sprite enemy
     * if(changeSprite) {
        spriteEnemy(enemy.getOrientation());
    }*/

    display.uvHG = enemy.getPosInTexture();                               //-----------//

    transformStruct.tran = enemy.getPosition();     // position dan s la fenetre de l'enemy
    transformStruct.scale = glm::vec2(enemy.getSize());    // taille en pixel du carre


    display.tran = libgl::transform(transformStruct, 0.5, 0.8);// coord a partir du coin HG

    // dessination du square
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //0 : premier vertex, 4 : quad = 4 vertices
    displayLife(enemy);

}


/**
 * Affichage de la barre de vie
 */
/*void Graphics::displayLife(Enemy &enemy) {
    if (enemy.getHp() != enemy.getHpMax()) {
        transformStruct.tran.y -= enemy.getSize() / 2;
        transformStruct.scale = glm::vec2(50, 10);
        Barre.tran = libgl::transform(transformStruct, 0.5, 0.5);
        Barre.hp = enemy.getHp();
        Barre.hpMax = enemy.getHpMax();
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        libgl::getError();
    }

}*/

void Graphics::displayBackground(int useTexture) {
    vao.bind();
    libgl::getError();
    if(!useTexture) {
        textureBackground.bindTexture();
        libgl::getError();
        display.texture2D = 0;
        libgl::getError();
    }
    display.uvHG = glm::vec2(0);
    libgl::getError();
    display.pictureSize = glm::vec2(1);
    display.useTexture = useTexture;
    libgl::getError();
    transformStruct.scale = glm::vec2(widthWindow, heightWindow);
    libgl::getError();
    transformStruct.tran = glm::vec2(0);
    libgl::getError();
    display.tran = libgl::transform(transformStruct);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}


/**
 * Image de l'enemy qui marche
 *
 * On utilise l'orientation de l'enemy pour afficher la bonne image
 */
void Graphics::spriteEnemy(int orientation, Enemy &enemy) {
    posHtextureEnemy = enemy.getPosInTexture().x;
    posVtextureEnemy = enemy.getPosInTexture().y;
    posHtextureEnemy += widthPictureEnemy;


    if (posHtextureEnemy > enemy.getBaseX()+3*widthPictureEnemy)
        posHtextureEnemy = enemy.getBaseX();
    switch (orientation) {
        case 0:
            spriteEnemyAhead(enemy);
            break;
        case 1:
            spriteEnemyLeft(enemy);
            break;
        case 2:
            spriteEnemyBehind(enemy);
            break;
        case 3:
            spriteEnemyRight(enemy);
            break;
        default:
            spriteEnemyAhead(enemy);
    }
    enemy.setPosInTexture(glm::vec2(posHtextureEnemy, posVtextureEnemy));
    /* std::cout << "--------------------- posHtextureEnemy : " << posHtextureEnemy << std::endl;
     std::cout << "--------------------- posVtextureEnemy : " << posVtextureEnemy << std::endl;*/
}

void Graphics::spriteEnemyAhead(const Enemy &enemy) {
    // position horizontale du sprite dans l'image


    // position verticale du sprite dans l'image
    posVtextureEnemy = enemy.getBaseY();
}

/**
 * L'enemy marche vers la gauche
 * On utilise la bonne ligne du fichier png.
 */
void Graphics::spriteEnemyLeft(const Enemy &enemy) {
    // position horizontale du sprite dans l'image


    // position verticale du sprite dans l'image
    posVtextureEnemy = enemy.getBaseY()+heightPictureEnemy;
}


/**
 * L'enemy marche vers la droite
 * On utilise la bonne ligne du fichier png.
 */
void Graphics::spriteEnemyRight(const Enemy &enemy) {
    // position horizontale du sprite dans l'image

    // position verticale du sprite dans l'image
    posVtextureEnemy = enemy.getBaseY() +2 * heightPictureEnemy;
}


/**
 * L'enemy marche de dos
 * On utilise la bonne ligne du fichier png.
 */
void Graphics::spriteEnemyBehind(const Enemy &enemy) {
    // position horizontale du sprite dans l'image

    // position verticale du sprite dans l'image


    posVtextureEnemy = enemy.getBaseY()+3 * heightPictureEnemy;
}
void Graphics::triangulize() {
    glm::vec2 vec;
    glm::vec2 normale;
    std::vector <glm::vec3> triangle;
    int pas = 1;
    float larg = 20;
    for (int i = 0 ; i < pointPath.size()-pas-1 ; i+=pas){
        vec = pointPath[i+pas] - pointPath[i];
        normale = larg*glm::normalize(glm::vec2{vec.y, -vec.x});
        triangle.push_back(glm::vec3{pointPath[i] + normale, 1.0f});
        normale = larg*glm::normalize(glm::vec2{-vec.y, vec.x});
        triangle.push_back(glm::vec3{pointPath[i] + normale, 1.0f});


    }
    numberPoint = (int)triangle.size();
    vaoPath.bind();
    vboPath = libgl::Vbo(0, 3, triangle);
    for (auto & tri :triangle){
        tri.x = tri.x / utils::widthWindow;
        tri.y = tri.y / utils::heightWindow;
    }
    vboCoordText = libgl::Vbo(1, 3, triangle);
    libgl::getError();



}
/*
 * Permet d'afficher le chemin on parcours le chemin et on affiche un cercle avec pour centre le point du chemin, mais lorsque on a une
 * ligne droite on affiche qu'un seul grand rectangle
 */
/**
 * Affichage du chemin des enemy
 *
 * Principe :
 *      On va afficher un rectangle de la taille de l'ecran autant de fois qu'on
 *      a stockée de cheminv = vec2(uvOut.x*width/height,uvOut.y);
 *      Et à chaque fois on va piocher dans la texture_menuTower seulement les points
 *      qui sont a une certaine distance du centre.
 */
void Graphics::displayPath() {
    vaoPath.bind();

    libgl::getError();
    texturePath.bindTexture();
    libgl::getError();
    pathEnemy.texture2D = 0;
    transformStruct.tran = glm::vec2{0};
    transformStruct.scale = glm::vec2{1};
    pathEnemy.tran = libgl :: transform(transformStruct,0.0,0.0);


    glDrawArrays(GL_TRIANGLE_STRIP, 0, numberPoint-2);


}


void Graphics::changeScale(const float x, const float y) {

    Range.scale = utils::normalized(widthWindow, heightWindow, x, y);
    display.scale = utils::normalized(widthWindow, heightWindow, x, y);
    Barre.scale = utils::normalized(widthWindow, heightWindow, x, y);
    pathEnemy.scale = utils::normalized(widthWindow, heightWindow, x, y);


}







