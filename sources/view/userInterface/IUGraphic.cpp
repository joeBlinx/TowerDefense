//
// Created by stiven on 15/02/16.
//

#include "view/userInterface/IUGraphic.hpp"
#include <iostream>
#include <GameState.hpp>
#include "utils.hpp"
#include "view/userInterface/Button.hpp"
#include "view/userInterface/SpellButton.hpp"

IUGraphic::IUGraphic() {

}

IUGraphic::IUGraphic(std::string pathTextureTower, std::string pathTextureSpell, int widthWindow, int heightWindow)
        : textureTower(pathTextureTower), textureSpell(pathTextureSpell), width(widthWindow), height(heightWindow) {

    vao.bind();
    vboRect = libgl::Vbo(0, 3, libgl::mat::rect);
    vboUv = libgl::Vbo(1, 2, libgl::mat::uv);

    /*Permet d'avoir un repère typé openGL dans le carré que l'on affiche
     * ne sert que pour l'affichage du cooldown des sorts */
    vboSpell = libgl::Vbo(2, 2, std::vector<glm::vec2> {
            {-1, 1},
            {-1, -1},
            {1,  1},
            {1,  -1}
    });

    // initialisation des uniformes
    program.scale = libgl::normalizedScreenSpace(widthWindow, heightWindow);
    program.bd = glm::vec2(decaleUv, decaleUv * 2);
    displayText.prog.scale = libgl::normalizedScreenSpace(widthWindow, heightWindow);
    interface.scale = libgl::normalizedScreenSpace(widthWindow, heightWindow);
    spellDisplay.scale = libgl::normalizedScreenSpace(widthWindow, heightWindow);
    spellDisplay.bd = glm::vec2(1/3.0f, 1/2.0f);


}

IUGraphic::IUGraphic(int widthWindow, int heightWindow) : width(widthWindow), height(heightWindow){
    displayText.prog.scale = libgl::normalizedScreenSpace(widthWindow, heightWindow);

}


void IUGraphic::displayTextButton(Button &button, std::string text) {

    //on affiche le bouton men
    displayText.prog.alpha = 1.0f;
    if (button.getSelected()) {
        displayText.prog.colorText = glm::vec3(0.50f);//mise en surbrillance
    } else {
        displayText.prog.colorText = glm::vec3(0.0f);//non surbrillance
    }
    libgl::display(displayText, text, glm::vec2(button.getPosition().x, button.getPosition().y),
                   glm::vec2(2*button.getSize()[0]/text.size(), (button.getSize()[1])));




}

void IUGraphic::displayTower(Button &button, int sound) {
    // on affiche les tours de l'IU
    vao.bind();
    if(!sound) {
        textureTower.bindTexture();
        program.hg = utils::calculUv(button);
        program.bd = glm::vec2(decaleUv, decaleUv * 2);
    }else{
        textureSound.bindTexture();
        program.hg = glm::vec2(0);
        program.bd = glm::vec2(0.5, 1.0f);
        if(!utils::sound.getPause()){
            program.hg = glm::vec2(0.5, 0);
        }
    }
    // on cacule la zone de la texture a utilisé en fonction du type de la tour
    if (button.getSelected()) {
        program.surbrillance = 0.50f;  //mise en surbrillance
    } else {
        program.surbrillance = 0.0f; //non surbrillance
    }
    transformStruct.tran = glm::vec2(button.getPosition().x, button.getPosition().y);
    transformStruct.scale = glm::vec2(button.getSize()[0], button.getSize()[1]);
    program.transformation = libgl::transform(transformStruct);
    program.texture2D = 0;
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

}

/* permet de changer la transparence de ce qu'on affiche
 * utilisé pour le placement des tours dans la map*/
void IUGraphic::changeAlpha(float alpha) {
    program.alpha = alpha;

}


void IUGraphic::changeColor(glm::vec3 color) {
    displayText.prog.couleur = color;

}

void IUGraphic::displayRect() {
    vao.bind();
    transformStruct.scale = glm::vec2(1.5 * (width / 16), height);
    transformStruct.tran = glm::vec2(width - 1.5 * width / 16, 0);
    interface.tran = libgl::transform(transformStruct);
    interface.end = 0;
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    transformStruct.angle = 90;
    transformStruct.scale = glm::vec2((width / 16), width);
    transformStruct.tran = glm::vec2(width, height - (width / 16));
    interface.tran = libgl::transform(transformStruct, 1.0, 1.0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    transformStruct.angle = 0;

}

void IUGraphic::displayStat(const StatPlayer &stat) {
    // affichage de la vie
    int sizeLetter = 20;
    int ecart = 3;
    std::string hp = "vie " + std::to_string(stat.hp);
    displayText.prog.alpha = 1.0f;
    displayText.prog.couleur = glm::vec3(0., 1., 1.);
    displayText.prog.colorText = glm::vec3(0);
    libgl::display(displayText, hp, glm::vec2(width / 2, 0), glm::vec2(sizeLetter));

    // affichage de la mana

    std::string mana = "mana " + std::to_string(stat.mana);
    displayText.prog.couleur = glm::vec3(0.7, 0.7, 0.);
    libgl::display(displayText, mana, glm::vec2(width / 2 + sizeLetter * (5 + ecart), 0), glm::vec2(sizeLetter));

    // affichage de l'argent

    std::string gold = "gold " + std::to_string(stat.gold);
    displayText.prog.couleur = glm::vec3(0., 0., 1.);
    libgl::display(displayText, gold, glm::vec2(width / 2 + sizeLetter * (10 + 2 * ecart), 0), glm::vec2(sizeLetter));


}

void IUGraphic::displaySpellButton(SpellButton &spellButton) {
    vao.bind();
    textureSpell.bindTexture();
    libgl::getError();
    spellDisplay.texture2D = 0;
    spellDisplay.wait = 0;
    libgl::getError();
    transformStruct.scale = spellButton.getSize();
    transformStruct.tran = spellButton.getPosition();
    spellDisplay.tran = libgl::transform(transformStruct);
    libgl::getError();
    spellDisplay.cooldown = spellButton.getCooldown();
    libgl::getError();
    spellDisplay.currentCooldown = spellButton.getCurrent();
    libgl::getError();
    spellDisplay.hg = chooseSpellImage(spellButton);
    spellDisplay.bd =  glm::vec2(1/3.0f, 1/3.0f);
    if (spellButton.getSelected() && !spellButton.isActive()) {
        spellDisplay.surbrillance = 0.30f;  //mise en surbrillance
    } else {
        spellDisplay.surbrillance = 0.0f; //non surbrillance
    }
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    libgl::getError();
}

void IUGraphic::displayCooldownWave(Button &button, float actualCool, float maxCool) {
    vao.bind();
    textureSpell.bindTexture();
    libgl::getError();
    spellDisplay.texture2D = 0;
    spellDisplay.wait = 1;
    libgl::getError();
    transformStruct.scale = button.getSize();
    transformStruct.tran = button.getPosition();
    spellDisplay.tran = libgl::transform(transformStruct);
    libgl::getError();
    spellDisplay.cooldown =maxCool;
    libgl::getError();
    spellDisplay.currentCooldown = actualCool;
    libgl::getError();
    spellDisplay.hg = glm::vec2{0.0f};
    spellDisplay.bd =  glm::vec2(1.0f);
    if (button.getSelected() ) {
        spellDisplay.surbrillance = 0.50f;  //mise en surbrillance
    } else {
        spellDisplay.surbrillance = 0.0f; //non surbrillance
    }
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    libgl::getError();

}


glm::vec2 IUGraphic::chooseSpellImage(SpellButton &spell) {
    glm::vec2 hg;
    constexpr float x = 1/3.0f;
    constexpr float y = 1.0f/3.0f;
    std::string key = spell.getKey();
    if(key == "meteore") {
    }else if (key =="freeze" ) {
        hg.x = x;
    }else if (key =="miroir" ) {
        hg.x = 2 * x;
    }else if (key == "earthQuake") {
        hg.y = y;
    }else if (key == "upgradeTower"){
            hg.y = y;
            hg.x = x;
    }else if (key == "glue"){
            hg.y = y;
            hg.x = 2*x;
    }else if(key == "light"){
        hg.y = 2*y;
        hg.x = 0;
    }
    return hg;

}

void IUGraphic::displayEnd(utils::state state, Button &end) {

    std::string str ;
    if(state == utils::state::LOSE){
        transformStruct.scale = glm::vec2(width, height);
        transformStruct.tran = glm::vec2 (0);
        interface.tran = libgl::transform(transformStruct);
        interface.alpha = alpha;
        interface.end = 1;
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        str = "YOU DIED";
        displayText.prog.couleur = glm::vec3{0.0, 1.0, 1.0};
    }else{
        str = "YOU WIN";
        displayText.prog.couleur = glm::vec3{1.0, 0.0, 1.0};
        alpha = 1.0f;
    }
    displayText.prog.alpha = alpha;
    displayText.prog.colorText = glm::vec3{0.0f};
    size_t sizeString = str.size();
    libgl::display(displayText, str, glm::vec2{utils::widthWindow/2 -50-sizeString*25,utils::heightWindow/2 -50}, glm::vec2{100, 50});
    if(alpha < 0.99)
        alpha+=0.009;
    else
       displayTextButton(end,"Resurect ?");

}







