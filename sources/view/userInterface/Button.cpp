//
// Created by stiven on 15/02/16.
//

#include <glm/glm.hpp>
#include <iostream>
#include "view/userInterface/Button.hpp"


Button::Button():coordinate(0,0),size(0,0),type(utils::element::NEUTRAL) {

}

Button::Button(float x, float y, int width, int height, utils::element type, std::function<void()> action) : coordinate(x, y), size(width, height), type(type), action(action){


}
Button::Button(float x, float y, int width, int height, utils::element type) : coordinate(x, y), size(width, height), type(type){

}

Button::Button(float x, float y, int width, int height, utils::element type, std::function<void()> action, bool text): Button(x, y, width, height, type, action) {
}



/* renvoie true si la souris se trouve sur le bouton
 * pour le moment la hitbox est carré*/
bool Button::isSelected(int xMouse, int yMouse) {

    return  (xMouse > coordinate.x && xMouse < coordinate.x + size[0]
            && yMouse > coordinate.y && yMouse < coordinate.y + size[1]);
}



glm::vec2 const &Button::getPosition() const {
    return coordinate;
}

glm::vec2 const &Button::getSize() const {
    return size;
}
//constructeur de déplacement
Button::Button(Button &&orig):  coordinate(orig.coordinate),size(orig.size), type(orig.type),action(orig.action){
        orig.coordinate = glm::vec2(0);
        orig.size = glm::vec2(0);
        orig.action = nullptr;
}

bool Button::getSelected() {
    return selected;
}

void Button::setSelected(bool select) {
    selected = select;

}

void Button::setCoor(float x, float y) {
    coordinate.x = x;
    coordinate.y = y;

}

const utils::element Button::getType() const {
    return type;
}

void Button::useButton() {
    if(clickable) {
        action();
        once = true;
    }

}

void Button::setY(float y) {
    setCoor(coordinate.x,y);
}


void Button::setClickable(bool click) {
    clickable = click;

}

void Button::changeFunction(std::function<void()> function) {
    action = function;
}



