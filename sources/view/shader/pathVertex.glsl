#version 330 core
layout (location = 0) in vec3 vertices;
layout (location = 1) in vec3 uv;

uniform mat3 scale;
uniform mat3 tran;

out vec2 uvOut;
out vec3 couleur;
void main() {

    gl_Position = vec4(scale*tran*vertices,1.);
    uvOut = (uv).xy;



}
