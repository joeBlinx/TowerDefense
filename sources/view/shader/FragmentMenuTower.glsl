#version 330 core

in vec2 uvFragment;
out vec4 color;
uniform sampler2D texture2D;
uniform float surbrillance;
uniform float alpha;

void main() {
    vec4 text = texture(texture2D, uvFragment).rgba;
    float a = alpha;
    if(text.a == 0.){
        a = 0.;
    }
    color = vec4(text.rgb, 1) + vec4(surbrillance, surbrillance, surbrillance, 0.);
    //color = vec4(text.rgb, a) + vec4(surbrillance, surbrillance, surbrillance, 0.);

   // color = vec4(0.2+surbrillance,0.5+surbrillance,0.3+surbrillance,1.);
   // color = vec4(surbrillance,surbrillance,surbrillance,1.);

  // color = vec4(1., 0., 0., 1.);
}
