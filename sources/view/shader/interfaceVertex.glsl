#version 330 core
layout (location = 0) in vec3 vertices;


uniform mat3 scale;
uniform mat3 tran;
void main() {

    gl_Position = vec4(scale*tran*vertices,1.);

}
