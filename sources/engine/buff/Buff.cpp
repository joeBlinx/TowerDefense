//
// Created by hole on 05/03/16.
//

#include <iostream>
#include "engine/buff/Buff.h"

Buff::Buff() {

}

Buff::Buff(int efficiency, int duration) : efficiency(efficiency), duration(duration) {

}

Buff::Buff(int efficiency, int duration, utils::element elem) : efficiency(efficiency), duration(duration), elem(elem) {

}

Buff::Buff(int duration, utils::element elem, glm::vec3 buffTower) : duration(duration), elem(elem), buffTower(buffTower) {
}

utils::element Buff::getElem(){
    return elem;
}

bool Buff::isOver() {
    return (duration<=0);
}

void Buff::decrementDuration() {

    duration--;
}


int Buff::getDuration() const {
    return duration;
}

int Buff::getEfficiency() {
    return efficiency;
}


Buff::Buff(int efficiency, int duration, utils::element elem, int next, int freq): efficiency(efficiency), duration(duration), elem(elem), frequency(freq), nextOccur(next){};//àsupr

glm::vec3 Buff::getBuffTower() {
    return buffTower;
}