#include <glm/detail/type_vec3.hpp>
#include <utils.hpp>
#include <engine/buff/BuffTower.hpp>

BuffTower::BuffTower(int duration, utils::element elem, glm::vec3 buffTower) : Buff(duration, elem, buffTower) {
}

BuffTower::BuffTower() {

}

void BuffTower::operator=(const BuffTower &orig) {
    duration = orig.duration;
    elem = orig.elem;
    buffTower = orig.buffTower;

}

