//
// Created by hole on 11/03/16.
//

#include "engine/path/SegBezier.h"
#include <iostream>

SegBezier::SegBezier() {

}

SegBezier::SegBezier(glm::vec2 b1, glm::vec2 b2, glm::vec2 b3, glm::vec2 b4):b1(b1), b2(b2), b3(b3), b4(b4){
    length = longueur();
}

float SegBezier::longueur(float bSup) {


    glm::vec2 newDot;
    glm::vec2 previousDot=b1;
    glm::vec2 diff;
    float length = 0.0f;
    for (float t=0; t <= bSup; t+= bSup/100) {

        newDot = calculPos(t);

        diff = newDot - previousDot;
        length += std::sqrt(diff.x * diff.x + diff.y * diff.y);


        previousDot = newDot;
    }

    newDot = calculPos(bSup);
    diff = newDot - previousDot;
    length += std::sqrt(diff.x * diff.x + diff.y * diff.y);

    return length;
}



glm::vec2 SegBezier::calculPos(float t) {

    glm::vec2 pos =b1 + t*((3.0f*(b2 - b1)) +
                           t*((3.0f*(b1-2.0f*b2+b3)) +
                              t*(-b1+3.0f*(b2-b3) +b4)));
    return pos;
}


glm::vec2 SegBezier::calculPosReele(float t){
    float s = 1/2.0f;
    float pas = 1/4.0f;
    float l=0.0;
    for(int i = 0; i < 30;i++){
        l = longueur(s);
        if(l<t){
            s=s+pas;
        }
        else{
            s=s-pas;
        }
        pas = pas*(1/2.0f);
    }
    return calculPos(s);
}


/*
void SegBezier::discretisator() {
    SegBezier c1({100, 100}, {180, 50}, {340, 200}, {300, 100});
}

// Returns the coefficients of derivative of a cubic bezie
glm::vec3 SegBezier::bezGetDerivative() {

    float c0 = (b2 - b3) * 9 + (b4 - b1) * 3;
    float c1 = (b1 + b3) * 6 - b2 * 12;
    float c2 = (b2 - b1) * 3;

    return {a, b, c};
}


//The arc-length of a parametric curve is
//
//  L(t) = Integral(sqrt(dx(t)^2 + dy(t)^2) dt)
//
// The slope of the function is dl/dt = sqrt(dx(t)^2 + dy(t)^2)
//
// If we have a few samples, L(ts) for every ts = (0, 1)
// We can use this to fit a monotone piecewise cubic function
// through all the samples to get a function g:t -> length
// We need the inverse function f:length -> t
// So we fit a monotone cubic on ts for every L(ts) with a slope of dt/dl


int SegBezier::getArcLengthParamerisationFn () {
    int nSamples = 20;


    glm::vec2 c0 = (b2 - b3) * 9 + (b4 - b1) * 3;
    glm::vec2 c1 = (b1 + b3) * 6 - b2 * 12;
    glm::vec2 c2 = (b2 - b1) * 3;

       // var ad = coeffsD[0]
        //var bd = coeffsD[1]
        //var cd = coeffsD[2]

        var lengths = []
            var slopes = []

            for (int i = 0; i < nSamples; ++i) {
                int ti = i / (nSamples - 1);

                // L(ti)
                int li = longueur(curveVals, 0, ti)
                lengths.push(li)

                // Slope of the length function (dl/dt) at ti
                var sti = (ad * ti + bd) * ti + cd
                var dl_dt = Math.sqrt(sti.x * sti.x + sti.y * sti.y)
                // THe slope we want, dt/dl
                slopes.push(dl_dt == 0 ? 0.0 : 1.0 / dl_dt)
            }

            // The monotone cubic code is a modified form of the sample given
            // at https://en.wikipedia.org/wiki/Monotone_cubic_interpolation

            // Precalculate the cubic interpolant coefficients
            var length = lengths[nSamples - 1]

            var nCoeff = nSamples - 1
            var dis = []  // degree 3 coeffiecients
            var cis = []  // degree 2 coeffiecients
            var li_prev = lengths[0]
            var tdi_prev = slopes[0]
            var step = 1.0 / nCoeff

            for (var i = 0; i < nCoeff; ++i) {
                var li = li_prev
                li_prev = lengths[i+1]
                var lDiff = li_prev - li
                var tdi = tdi_prev
                var tdi_next = slopes[i+1]
                tdi_prev = tdi_next
                var si = step / lDiff
                var di = (tdi + tdi_next - 2 * si) / (lDiff * lDiff)
                var ci = (3 * si - 2 * tdi - tdi_next) / lDiff
                dis.push(di)
                cis.push(ci)
            }

            // The arc-length reparametrisation function for the given curve
            // given a 'value', returns the approximate 't' for which the arc-length
            // from the start point of the curve equals 'value'.
            var arcLenParam = function (len) {
                if (len >= length) {
                    return 1.0
                }

                if (len <= 0) {
                    return 0.0
                }

                // Find the cubic segment which has 'len'
                var left = 0
                var right = nCoeff

                while (left <= right) {
                    var mid = Math.floor((left + right) / 2)
                    var lMid = lengths[mid]

                    if (lMid < len) {
                        left = mid + 1
                    } else if (lMid > len) {
                        right = mid - 1
                    } else {
                        return mid * step
                    }
                }

                var i = Math.max(0, right)
                var ti = i * step
                var tdi = slopes[i]
                var di = dis[i]
                var ci = cis[i]
                var ld = len - lengths[i]

                return ((di * ld + ci) * ld + tdi) * ld + ti
            }

            return arcLenParam
        }



    var getTForLength = getArcLengthParamerisationFn(c1)

    console.log("Mid point on the curve is at t =", getTForLength(p1.length/2))

// Sample curves at a distance of 10px apart
    var ti = 0
    var len = 0

    while (ti < 1.0) {
        ti = getTForLength(len)
        len += 10

        var pnt = c1.getPointAt(ti, true)
        var pc = new Path.Circle(pnt, 1)
        pc.fillColor = "black"
    }
}
*/