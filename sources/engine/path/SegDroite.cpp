//
// Created by hole on 12/03/16.
//


#include "engine/path/SegDroite.h"
#include<cmath>

glm::vec2 SegDroite::calculPos(float t) {
    return (Pos1 + t*(Pos2-Pos1));
}

float SegDroite::longueur() {
    return std::sqrt((Pos1.x - Pos2.x) * (Pos1.x - Pos2.x) + (Pos1.y - Pos2.y) * (Pos1.y - Pos2.y));
}

SegDroite::SegDroite() {

}

SegDroite::SegDroite(glm::vec2 Pos1, glm::vec2 Pos2):Pos1(Pos1),Pos2(Pos2) {
    length = longueur();
}

glm::vec2 SegDroite::calculPosReele(float t) {
    return (Pos1 + (t/length)*(Pos2-Pos1));
}
