//
// Created by hole on 29/03/16.
//

#include <engine/spell/OneClicMiroir.hpp>
#include <engine/projectile/Miroir.hpp>

bool OneClicMiroir::apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
                          const std::vector<std::unique_ptr<Enemy>> &enemies,
                          std::vector<std::unique_ptr<Projectile>> &projectiles) {

                        projectiles.push_back(std::make_unique<Miroir>(path,damage));
    return true;


}


OneClicMiroir::OneClicMiroir(int price, int coolDown, utils::element element, float damage, Path &path) :
        Spell(price,coolDown,element, damage),path(path){};




