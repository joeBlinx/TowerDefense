//
// Created by jozereau on 21/03/16.
//

#include "engine/map/Object.hpp"

Object::Object(bool breakable, float price, float modifier, int type) :
        breakable(breakable), price(price), modifier(modifier), type(type) {
}

const bool Object::getBreakable() {
    return breakable;
}

const float Object::getPrice() {
    return price;
}

const float Object::getModifier() {
    return modifier;
}

const int Object::getType() {
    return type;
}