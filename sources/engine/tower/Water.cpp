//
// Created by jozereau on 15/02/16.
//

#include <glm/detail/func_geometric.hpp>
#include <engine/buff/Slower.h>
#include "engine/tower/Water.hpp"
#include "engine/projectile/Projectile.h"

Water::Water():Tower() {

    type = utils::element::WATER;
}

Water::Water(glm::vec2 pos) :
        Tower(pos, utils::element::WATER, 90.0f, 10.0f, 6.0f, 10.0f) {
    buff.push_back(std::make_unique <Slower> (1, 40));
}

std::unique_ptr <Projectile> Water::shoot(Path &path, float gameSpeed) {
    int time = 0;
    glm::vec2 posFut, direction;
    float prog = target->getProgression();
    bool find = false;

    for (time=0;time<100 && !find;time++) {
        prog += target->getSpeed() * gameSpeed;
        if (prog < path.getTotalLength()){
            posFut = path.getPosition(prog);
            direction = glm::normalize(posFut - pos);

            if (glm::distance(posFut, vit * time * direction+pos) <= target->getSize()/4) {
                find = true;
            }
        }
    }


    return std::make_unique <Projectile> (pos, vit*direction, damage, buff, type, (int)glm::length(posFut-pos)/glm::length(2.0f*direction) );
}